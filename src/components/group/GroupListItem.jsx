import React from 'react';
import Popup from 'reactjs-popup';
import {withRouter} from 'react-router-dom';
import '../../css/group/my-group.css';
import logo from '../../logo.svg';

class GroupListItem extends React.Component {
  _renderMenu = menu => (
    <div className="but">
      <Popup
        trigger={<i className="material-icons">more_vert</i>}
        position="bottom right"
        on="click"
        contentStyle={{
          borderWidth: '1px',
          borderRadius: '5px',
          width: 'fit-content',
          boxShadow: 'none',
        }}
        closeOnDocumentClick>
        <div className="menu">
          {menu.map((item, index) => {
            return (
              <div key={index} onClick={item.function} className="menu-item">
                {item.name}
              </div>
            );
          })}
        </div>
      </Popup>
    </div>
  );

  render() {
    return (
      <div className="item">
        <div className="image">
          <img
            src={
              this.props.item.avatar && this.props.item.avatar !== ''
                ? this.props.item.avatar
                : logo
            }
            alt=""
          />
        </div>
        <div className="content">
          <h3
            onClick={() => {
              this.props.history.push(this.props.item.link);
            }}>
            {this.props.item.name}
          </h3>
          <h4>{this.props.item.description}</h4>
        </div>
        {this._renderMenu(this.props.menu)}
      </div>
    );
  }
}

export default withRouter(GroupListItem);
