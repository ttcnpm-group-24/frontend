import React from "react";
import ScreenWrapper from "../components/general/ScreenWrapper";
import { withGlobalContext } from "../components/contexts/GlobalContext";
import PostScreen from "./post/PostScreen";

class HomeScreen extends React.Component {
  render() {
    return (
      <ScreenWrapper
        sidebar_name={this.props.global.user.name}
        sidebar_ava={this.props.global.user.avatar}
        sidebar_des={this.props.global.user.description}
        sidebar_buttons={this.props.global.getButtons()}
        content={<PostScreen />}
      />
    );
  }
}

export default withGlobalContext(HomeScreen);
