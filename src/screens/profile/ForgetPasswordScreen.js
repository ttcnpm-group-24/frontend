import React from "react";
import { withGlobalContext } from "../../components/contexts/GlobalContext";
import { withRouter } from "react-router-dom";
import ScreenWrapper from "../../components/general/ScreenWrapper";
import "../../css/profile/forget.css";

class ForgetPasswordScreen extends React.Component {
      constructor(props) {
        super(props);
        this.state = {
          username: ""
        };
      }
      cancel = () => {
        this.props.history.push("/");
      };
      getInput = username => {
        if (username === "username") {
          return this.state.username;
        }
      };
      renderContent = () => {
        return (
          <div className="forget-password">
            <div className="dialog">
              <div className="info">
                <div className="content">
                  <h1>Please fill your information</h1>
                </div>
                <div className="content">
                  <div className="title">
                    <h4>Username</h4>
                  </div>
                  <input
                    type="text"
                    name="username"
                    placeholder="Enter your username"
                  />
                </div>
                <div className="content">
                  <div className="title">
                    <h4>Email</h4>
                  </div>
                  <input
                    type="text"
                    name="email"
                    placeholder="Enter your email"
                  />
                </div>
                <div className="content" id="buttons">
                  <button className="cancel" onClick={this.cancel}>
                    Cancel
                  </button>
                  <button className="submit">Submit</button>
                </div>
                
              </div>
            </div>
          </div>
        );
      };
      render() {
        return (
          <ScreenWrapper
            sidebar_name={this.props.global.user.name}
            sidebar_ava={this.props.global.user.ava}
            sidebar_des={this.props.global.user.des}
            sidebar_buttons={this.props.global.getButtons()}
            content={this.renderContent()}
          />
        );
      }
    }

export default withGlobalContext(withRouter(ForgetPasswordScreen));
