let _navigator;

function setTopLevelNavigator(navigationRef) {
  _navigator = navigationRef;
}

function navigate(routeName) {
  _navigator.history.push(routeName);
}

function replace(routeName) {
  _navigator.history.replace(routeName);
}

export default {
  navigate,
  replace,
  setTopLevelNavigator
};
