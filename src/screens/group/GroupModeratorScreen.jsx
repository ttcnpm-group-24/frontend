import React from "react";
import GroupScreenWrapper from "../../components/group/GroupScreenWrapper";
import { withGlobalContext } from "../../components/contexts/GlobalContext";
import "../../css/group/my-group.css";
import GroupListItem from "../../components/group/GroupListItem";
import GroupModApi from "../../controller/group/GroupModApi";
import GroupMemberApi from "../../controller/group/GroupMemberApi";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { confirmAlert } from "react-confirm-alert";
import GroupApi from "../../controller/group/GroupApi";
import role from "../../controller/general/config";

class GroupModeratorScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      data: [],
      open: false,
      email: null
    };
  }
  getMods = () => {
    GroupModApi.getModerator(this.state.id, this.props.global.access_token)
      .then(data => {
        let mods = [];
        data.forEach(element => {
          let el = element.client;
          el.link = "/clients/" + element.client.id;
          mods.push(el);
        });
        this.setState({ data: mods });
      })
      .catch(e => {
        console.error(e);
      });
  };
  renderContent = (item, index) => {
    return (
      <GroupListItem
        key={index}
        item={item}
        menu={[
          {
            name: "Delete from group",
            function: () => {
              this.removeSelectedUser(item.id);
            }
          },
          {
            name: "Profile",
            function: () => {
              this.props.history.push("/clients/" + item.id);
            }
          },
          {
            name: "Remove moderator",
            function: () => {
              this.removeMod(item.id);
            }
          }
        ]}
      />
    );
  };
  componentWillMount() {
    this.props.global.updateCurrentGroup(this.state.id);
    this.getMods();
  };
  openDialog = () => {
    this.setState({ open: true });
  };
  closeDialog = () => {
    this.setState({ open: false });
  };
  addModerator = () => {
    if (this.state.email && this.state.email !== "") {
      GroupMemberApi.findMember(
        {
          email: this.state.email,
          group_id: this.props.global.current_group.id
        },
        this.props.global.access_token
      ).then(res => {
        if (res.length === 0) {
          alert("Member not found");
          return;
        } else {
          GroupMemberApi.setMemberToModerator(
            {
              user_id: res[0].id,
              group_id: this.props.global.current_group.id
            },
            this.props.global.access_token
          ).then(res2 => {
            if (res2.error) {
              alert("Unable to add moderator");
            } else {
              this.getMods();
            }
          });
        }
      });
      this.closeDialog();
    } else {
      alert("Please enter email address");
    }
  };
  render() {
    return (
      <GroupScreenWrapper
        id={this.state.id}
        isAdmin={this.props.global.isAdmin}
        name={this.props.global.current_group.name}
        ava={this.props.global.current_group.avatar}
        des={this.props.global.current_group.description}
        content={
          <div className="my-group-wrapper">
            <div className="title">
              <h2>Moderators of {this.props.global.current_group.name}</h2>
              <button onClick={this.openDialog}>Add moderator</button>
            </div>
            <div className="my-group">
              {this.state.data.map((item, index) => {
                return this.renderContent(item, index);
              })}
            </div>
            <Dialog
              open={this.state.open}
              onClose={this.closeDialog}
              aria-labelledby="form-dialog-title"
            >
              <DialogTitle id="form-dialog-title">Add moderator</DialogTitle>
              <DialogContent>
                <DialogContentText>
                  To make a member a moderator of{" "}
                  {this.props.global.current_group.name}, enter that member's
                  email here.
                </DialogContentText>
                <TextField
                  autoFocus
                  margin="dense"
                  label="Email Address"
                  type="email"
                  fullWidth
                  onChange={e => {
                    this.setState({ email: e.target.value });
                  }}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={this.closeDialog} color="primary">
                  Cancel
                </Button>
                <Button onClick={this.addModerator} color="secondary">
                  Submit
                </Button>
              </DialogActions>
            </Dialog>
          </div>
        }
      />
    );
  }
  removeSelectedUser = user_id => {
    confirmAlert({
      title: "Confirm to remove",
      message: "Are you sure to remove this moderator from group?",
      buttons: [
        {
          label: "Yes",
          onClick: () => {
            GroupMemberApi.deleteMember(
              {
                user_id: user_id,
                group_id: this.props.global.current_group.id
              },
              this.props.global.access_token
            )
              .then(res => {
                if (res.error) {
                  alert(
                    "Unable to remove this user from " +
                      this.props.global.current_group.name
                  );
                  return;
                } else {
                  this.getMods();
                }
              })
              .catch(e => {
                console.error(e);
              });
          }
        },
        {
          label: "No"
        }
      ]
    });
  };
  removeMod = user_id => {
    confirmAlert({
      title: "Confirm to remove",
      message: "Are you sure to make this moderator normal user?",
      buttons: [
        {
          label: "Yes",
          onClick: () => {
            GroupModApi.setModeratorToMember(
              {
                user_id: user_id,
                group_id: this.props.global.current_group.id
              },
              this.props.global.access_token
            )
              .then(res => {
                if (res.error) {
                  alert("Unable to change this mod to normal user");
                  return;
                } else {
                  this.getMods();
                }
              })
              .catch(e => {
                console.error(e);
              });
          }
        },
        {
          label: "No"
        }
      ]
    });
  };
}

export default withGlobalContext(GroupModeratorScreen);
