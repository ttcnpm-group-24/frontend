import React from "react";
import { withRouter } from "react-router-dom";
import { withGlobalContext } from "..//contexts/GlobalContext";
import "../../css/post/list-post.css";
import Popup from "reactjs-popup";
import PostApi from "../../controller/post/PostApi";
import logo from "../../logo.svg";
//import { withGlobalContext } from "../contexts/GlobalContext";
class Post extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      vote: 0,
      user_name: ""
    }
  }
  _renderMenu = menu => (
    <div className="but">
      <Popup
        trigger={<i className="material-icons">more_vert</i>}
        position="bottom right"
        on="click"
        contentStyle={{
          borderWidth: "1px",
          borderRadius: "5px",
          width: "fit-content",
          boxShadow: "none"
        }}
        closeOnDocumentClick
      >
        <div className="menu">
          {menu.map((item, index) => {
            return (
              <div key={index} onClick={item.function} className="menu-item">
                {item.name}
              </div>
            );
          })}
        </div>
      </Popup>
    </div>
  );
  vote = () => {
    console.log(this.state.vote)
    if(this.state.vote < 1){
      console.log(this.state.vote)
      this.setState({vote: this.state.vote + 1})
      return true;
    }
    //return true;
  }

  componentWillMount = () => {
    PostApi.getUserName(this.props.item.user_id,this.props.global.access_token)
    .then(data => {
      this.setState({user_name: data.name})
    })
  }
  render() {
    
    return (
      <div className="item">
        <div className="vote">
          <div className="up">
            <button class="btn" 
                    onClick={this.props.up}>
              <i class="fas fa-angle-up"></i>
            </button>
          </div>
          <div>
            {this.props.item.upvote}
          </div>
          <div className="up">
            <button class="btn" onClick={this.props.down}>
              <i class="fas fa-angle-down"></i>
            </button>
          </div>
        </div>
        <div className="image">
          <img src={logo} alt=""/>
        </div>
        <div className="content1">
            <div>
              <h1
                  onClick={() => {
                  this.props.history.push(this.props.item.link);
                  }}
              >{this.props.item.title}</h1>
            </div>
            <div className="pre">
              <pre>{this.props.des}</pre>
            </div>
            <div className="author-time">
              <div className="author">
                <span>Posted by </span><div className="name"><a>{this.state.user_name}</a></div>
              </div>
              <div className="time">
                <div className="name"><a>{this.props.time}</a></div>
              </div>
            </div>
        </div>
        {this._renderMenu(this.props.menu)}
      </div>
    );
  }
}

export default withGlobalContext(withRouter(Post));
