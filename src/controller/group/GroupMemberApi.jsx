import makeRequest from '../general/api';
import role from '../general/config';
import {
  url_wrapper,
  addToGroup,
  removeUserFromGroup,
  changeRoleInGroup,
} from './GroupApi';

export default class GroupMemberApi {
  static getMember = (group_id, access_token) => {
    /**
     * group_id
     * role: 0 - normal, 1 - mod, 2 - owner
     */
    const url = url_wrapper('/Groups/' + group_id + '/users', access_token);
    return makeRequest(url, {
      method: 'GET',
    });
  };

  static findUser = (email, access_token) => {
    const filter = JSON.stringify({
      where: {
        email: email,
      },
    });
    return makeRequest(url_wrapper('/Clients?filter=' + filter, access_token), {
      method: 'GET',
    });
  };

  static findMember = (data, access_token) => {
    /*
     * email
     * group_id
     */
    const filter = JSON.stringify({
      where: {
        email: data.email,
      },
    });
    return makeRequest(
      url_wrapper(
        '/Groups/' + data.group_id + '/users?filter=' + filter,
        access_token,
      ),
      {
        method: 'GET',
      },
    );
  };

  static addMember = (data, access_token) => {
    /**
     * user_id
     * group_id
     */
    return addToGroup(
      {
        role: role.normal,
        user_id: data.user_id,
        group_id: data.group_id,
      },
      access_token,
    );
  };

  static deleteMember = (data, access_token) => {
    /**
     * user_id
     * group_id
     */
    return removeUserFromGroup(data, access_token);
  };

  static setMemberToOwner = (data, access_token) => {
    /**
     * user_id
     * group_id
     */
    return changeRoleInGroup(
      {
        role: role.owner,
        user_id: data.user_id,
        group_id: data.group_id,
      },
      access_token,
    );
  };

  static setMemberToModerator = (data, access_token) => {
    /**
     * user_id
     * group_id
     */
    return changeRoleInGroup(
      {
        role: role.moderator,
        user_id: data.user_id,
        group_id: data.group_id,
      },
      access_token,
    );
  };

  static checkUserInGroup = (data, access_token) => {
    /*
     * user_id
     * group_id
     */
    return makeRequest(
      url_wrapper('/Groups/' + data.group_id + '/users/rel/' + data.user_id),
      {
        method: 'HEAD',
      },
    );
  };
}
