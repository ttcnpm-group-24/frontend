import React from 'react';
import GroupApi from '../../controller/group/GroupApi';
import role from '../../controller/general/config';
import ProfileApi from '../../controller/profile/ProfileApi';
import GroupMemberApi from '../../controller/group/GroupMemberApi';

const globalContext = React.createContext();

export default class GlobalContextProvider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      access_token: '',
      current_group: {},
      isAdmin: false,
      isOwner: false,
      isMember: false,
      foundGroup: [],
      loading: false,
    };
  }
  componentWillMount() {
    const user = localStorage.getItem('user_ttcnpm')
      ? JSON.parse(localStorage.getItem('user_ttcnpm'))
      : {};
    const access_token = localStorage.getItem('access_token_ttcnpm')
      ? localStorage.getItem('access_token_ttcnpm')
      : '';
    const current_group = localStorage.getItem('current_group_ttcnpm')
      ? JSON.parse(localStorage.getItem('current_group_ttcnpm'))
      : {};
    const isAdmin =
      localStorage.getItem('isadmin_ttcnpm') === 'true' ? true : false;
    const isOwner =
      localStorage.getItem('isowner_ttcnpm') === 'true' ? true : false;
    const isMember =
      localStorage.getItem('ismember_ttcnpm') === 'true' ? true : false;
    this.setState({
      user: user,
      access_token: access_token,
      current_group: current_group,
      isAdmin: isAdmin,
      isOwner: isOwner,
      isMember: isMember,
    });
  }
  setLoading = loading => {
    this.setState({loading: loading});
  };
  getButtons = () => {
    var buttons = [
      {
        name: 'Home',
        link: '/',
      },
    ];
    if (this.state.access_token) {
      buttons = buttons.concat([
        {
          name: 'My Groups',
          link: '/my-groups',
        },
        {
          name: 'My Profile',
          link: '/my-profile',
        },
        {
          name: 'Change Password',
          link: '/change-password/' + this.state.user.id,
        },
        {
          name: 'Find Groups',
          link: '/find-groups',
        },
      ]);
    }
    return buttons;
  };
  setUser = user => {
    localStorage.setItem('user_ttcnpm', JSON.stringify(user));
    this.setState({user: user});
  };
  setAccessToken = (token, id) => {
    this.setState({access_token: token});
    ProfileApi.getUser(id, this.state.access_token).then(res => {
      if (res.error) {
        alert('Something went wrong');
        this.logout();
      } else {
        localStorage.setItem('access_token_ttcnpm', token);
        this.setUser(res);
      }
    });
  };
  logout = () => {
    this.setState({user: {}, access_token: ''});
    localStorage.clear();
  };
  getGroup = group_id => {
    return GroupApi.getAGroup(group_id, this.state.access_token)
      .then(data => {
        console.log(data);
        if (!data) {
          alert('Something went wrong.');
          return;
        }
        // Clear cache to re-render image
        if (data.avatar) {
          data.avatar += '?v=' + Date.now();
        }
        localStorage.setItem('current_group_ttcnpm', JSON.stringify(data));
        this.setState({current_group: data});
        GroupApi.getRoleInGroup(
          {
            user_id: this.state.user.id,
            group_id: this.state.current_group.id,
          },
          this.state.access_token,
        ).then(r => {
          if (r === role.moderator || r === role.owner) {
            localStorage.setItem('isadmin_ttcnpm', true);
            this.setState({isAdmin: true});
            if (r === role.owner) {
              localStorage.setItem('isowner_ttcnpm', true);
              this.setState({isOwner: true});
            } else {
              localStorage.setItem('isowner_ttcnpm', false);
              this.setState({isOwner: false});
            }
          } else {
            localStorage.setItem('isadmin_ttcnpm', false);
            localStorage.setItem('isowner_ttcnpm', false);
            this.setState({isAdmin: false, isOwner: false});
          }
          GroupMemberApi.checkUserInGroup({
            user_id: this.state.user.id,
            group_id: this.state.current_group.id,
          }).then(res => {
            if (res === 200) {
              localStorage.setItem('ismember_ttcnpm', true);
              this.setState({isMember: true});
            } else {
              localStorage.setItem('ismember_ttcnpm', false);
              this.setState({isMember: false});
            }
          });
        });
      })
      .catch(e => {
        console.error(e);
      });
  };
  updateCurrentGroup = group_id => {
    if (
      JSON.stringify(this.state.current_group) === '' ||
      parseInt(this.state.current_group.id) !== parseInt(group_id)
    ) {
      this.setLoading(true);
      this.getGroup(group_id).then(() => {
        this.setLoading(false);
      });
    }
  };
  getAllGroup = () => {
    return GroupApi.getAllGroup(this.state.access_token).then(res => {
      res.map(item => {
        item.link = '/groups/home/' + item.id;
      });
      this.setState({foundGroup: res});
    });
  };
  render() {
    return (
      <globalContext.Provider
        value={{
          ...this.state,
          getButtons: this.getButtons,
          setUser: this.setUser,
          setAccessToken: this.setAccessToken,
          updateCurrentGroup: this.updateCurrentGroup,
          getGroup: this.getGroup,
          logout: this.logout,
          getAllGroup: this.getAllGroup,
          setLoading: this.setLoading,
        }}>
        {this.props.children}
      </globalContext.Provider>
    );
  }
}

export const withGlobalContext = ChildComponent => props => (
  <globalContext.Consumer>
    {context => <ChildComponent {...props} global={context} />}
  </globalContext.Consumer>
);
