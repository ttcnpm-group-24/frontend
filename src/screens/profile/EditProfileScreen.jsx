import React from "react";
import ScreenWrapper from "../../components/general/ScreenWrapper";
import { withGlobalContext } from "../../components/contexts/GlobalContext";
import { withRouter } from "react-router-dom";
import "../../css/profile/edit-profile.css";
import logo from "../../logo.svg";

class EditProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      name: "",
      ava: logo
    };
  }
  cancel = () => {
    this.props.history.push("/");
  };
  getInput = name => {
    if (name === "name") {
    } else if (name === "des") {
    } else if (name === "ava") {
      return this.state.ava;
    }
  };
  renderContent = () => {
    return (
      <div className="edit-profile">
        <div className="dialog">
          <div className="info">
            <div className="content">
              <h1>Edit Profile</h1>
            </div>
            <div className="content">
              <div className="title">
                <h4>Full Name:</h4>
              </div>
              <input
                type="text"
                name="full-name"
                placeholder="Enter your full name"
              />
            </div>
            <div className="content">
              <div className="title">
                <h4>Description:</h4>
              </div>
              <input
                type="text"
                name="des"
                placeholder="Enter your description"
              />
            </div>
            <div className="content">
              <div className="title">
                <h4>Gender:</h4>
              </div>
              <input
                type="text"
                name="gender"
                placeholder="Enter your gender"
              />
            </div>
            <div className="content">
              <div className="title">
                <h4>Address :</h4>
              </div>
              <input
                type="text"
                name="address"
                placeholder="Enter your address"
              />
            </div>
            <div className="content">
              <div className="title">
                <h4>Avatar:</h4>
              </div>
              <div className="input-image">
                <label htmlFor="profile-ava">Choose an image</label>
                <input
                  type="file"
                  name="profile-ava"
                  accept="image/x-png,image/jpeg"
                  onChange={e => {
                    if (e.target.files && e.target.files.length) {
                      this.setState({
                        group_ava: URL.createObjectURL(e.target.files[0])
                      });
                    }
                  }}
                />
              </div>
            </div>
            <div className="content">
              <div className="title">
                <h4>Password:</h4>
              </div>
              <input
                type="password"
                name="password"
                placeholder="Confirm your current password"
              />
            </div>
            <div className="content" id="buttons">
              <button className="cancel" onClick={this.cancel}>
                Cancel
              </button>
              <button className="submit">Change</button>
            </div>
          </div>
        </div>
      </div>
    );
  };
  render() {
    return (
      <ScreenWrapper
        sidebar_name={this.props.global.user.name}
        sidebar_ava={this.props.global.user.ava}
        sidebar_des={this.props.global.user.des}
        sidebar_buttons={this.props.global.getButtons()}
        content={this.renderContent()}
      />
    );
  }
}

export default withGlobalContext(withRouter(EditProfileScreen));
