import React from "react";
import { confirmAlert } from "react-confirm-alert";
import "../../css/group/setting-group.css";
import "react-confirm-alert/src/react-confirm-alert.css"; // Import css
import { withGlobalContext } from "../contexts/GlobalContext";
import GroupApi from "../../controller/group/GroupApi";
import { server_url } from "../../controller/general/config";
import Navigator from "../general/Navigator";

const Compress = require("compress.js");
const compress = new Compress();

class GroupAvatar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      change: false,
      group_ava: null,
      group_ava_display: server_url + this.props.global.current_group.avatar
    };
  }
  _cancel = () => {
    confirmAlert({
      title: "Confirm to cancel",
      message: "Are you sure to cancel?",
      buttons: [
        {
          label: "Yes",
          onClick: () => {
            this.setState({
              change: false,
              group_ava_display:
                server_url + this.props.global.current_group.avatar
            });
          }
        },
        {
          label: "No"
        }
      ]
    });
  };
  _save = () => {
    confirmAlert({
      title: "Confirm to save",
      message: "Are you sure to change avatar?",
      buttons: [
        {
          label: "Yes",
          onClick: () => {
            GroupApi.updateAvatar(
              {
                group_id: this.props.global.current_group.id,
                avatar: this.state.group_ava
              },
              this.props.global.access_token
            ).then(res => {
              this.setState({ change: false });
              this.props.global
                .getGroup(this.props.global.current_group.id)
                .then(() => {
                  Navigator.replace(
                    "/groups/settings/" + this.props.global.current_group.id
                  );
                });
            });
          }
        },
        {
          label: "No"
        }
      ]
    });
  };
  render() {
    return (
      <div className="group-info">
        <div className="group-info-title">
          <h2>Group's Avatar</h2>
          {this.state.change ? (
            <div className="buttons">
              <button className="but save" onClick={this._save}>
                Save
              </button>
              <button className="but" onClick={this._cancel}>
                Cancel
              </button>
            </div>
          ) : (
            <button
              className="but edit"
              onClick={() => {
                this.setState({ change: true });
              }}
            >
              Change
            </button>
          )}
        </div>
        {this.state.change ? (
          <div className="group-info-item">
            <div className="avatar">
              <img src={this.state.group_ava_display} alt="group-ava" />
              <div className="img-selection">
                <h4>Avatar</h4>
                <p>Set your group's profile picture by uploading an image</p>
                <input
                  type="file"
                  name="group-ava"
                  accept="image/x-png,image/jpeg"
                  onChange={e => {
                    if (e.target.files && e.target.files.length) {
                      const images = [...e.target.files];
                      compress
                        .compress(images, {
                          size: 1,
                          quality: 1
                        })
                        .then(data => {
                          const img = data[0];
                          const base64str = img.data;
                          const imgExt = img.ext;
                          const file = Compress.convertBase64ToFile(
                            base64str,
                            imgExt
                          );
                          this.setState({
                            group_ava_display: URL.createObjectURL(file),
                            group_ava: file
                          });
                        });
                    }
                  }}
                />
              </div>
            </div>
          </div>
        ) : (
          <div />
        )}
      </div>
    );
  }
}
export default withGlobalContext(GroupAvatar);
