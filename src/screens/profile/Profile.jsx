import React from 'react';
import ScreenWrapper from '../../components/general/ScreenWrapper';
import {withGlobalContext} from '../../components/contexts/GlobalContext';
import logo from '../../logo.svg';
import '../../css/profile/profile.css';
import ProfileApi from '../../controller/profile/ProfileApi';
import {server_url} from '../../controller/general/config';

class ProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      name: '',
      description: '',
      address: '',
      gender: '',
      email: '',
      username: '',
      avatar_display: logo,
    };
  }
  getUser = () => {
    ProfileApi.getUser(this.state.id, this.props.global.access_token).then(
      res => {
        this.setState({
          name: res.name,
          description: res.description,
          address: res.address,
          gender: res.gender,
          email: res.email,
          username: res.username,
          avatar_display: res.avatar ? server_url + res.avatar : logo,
        });
      },
    );
  };
  componentDidMount() {
    this.getUser();
  }
  render() {
    return (
      <ScreenWrapper
        sidebar_name={this.props.global.user.name}
        sidebar_ava={this.props.global.user.avatar}
        sidebar_des={this.props.global.user.description}
        sidebar_buttons={this.props.global.getButtons()}
        content={this.renderContent()}
      />
    );
  }
  renderContent = () => {
    return (
      <div className="edit-profile">
        <h2 className="page-title">Profile of {this.state.name}</h2>
        <div className="avatar">
          <img src={this.state.avatar_display} alt="ava" />
          <div className="img-selection">
            <h4>Avatar</h4>
            <p>This is the avatar of {this.state.name}</p>
          </div>
        </div>
        <h4 className="page-title">Name</h4>
        <div className="input">
          <input
            type="text"
            name="name"
            placeholder="Enter your name here"
            value={this.state.name}
            disabled={true}
          />
        </div>
        <h4 className="page-title">Description</h4>
        <div className="input">
          <textarea
            type="text"
            name="group-name"
            placeholder="Enter your description here"
            value={this.state.description}
            disabled={true}
          />
        </div>
        <h4 className="page-title">Address</h4>
        <div className="input">
          <input
            type="text"
            name="address"
            placeholder="Enter your address here"
            value={this.state.address}
            disabled={true}
          />
        </div>
        <h4 className="page-title">Email</h4>
        <div className="input">
          <input
            type="text"
            name="email"
            placeholder="Enter your email here"
            value={this.state.email}
            disabled={true}
          />
        </div>
        <h4 className="page-title">Username</h4>
        <div className="input">
          <input
            type="text"
            name="username"
            placeholder="Enter your username here"
            value={this.state.username}
            disabled={true}
          />
        </div>
      </div>
    );
  };
}

export default withGlobalContext(ProfileScreen);
