import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import ProfileApi from "../../controller/profile/ProfileApi";

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      gender: "",
      username: "",
      address: "",
      description: "",
      email: "",
      password: "",
      retype_password: ""
    };
  }
  Register = () => {
    ProfileApi.Register({
      name: this.state.name,
      description: this.state.description,
      address: this.state.address,
      username: this.state.username,
      email: this.state.email,
      password: this.state.password,
      gender: this.state.gender
    }).then(res => {
      if (res.error) {
        alert("Something went wrong");
      } else {
        alert("Register successfully");
        this.props.closeDialog();
      }
    });
  };
  render() {
    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.closeDialog}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Register</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Enter the following to create an account.
          </DialogContentText>
          <TextField
            margin="dense"
            label="Name"
            type="text"
            fullWidth
            onChange={e => {
              this.setState({ name: e.target.value });
            }}
          />
          <TextField
            margin="dense"
            label="Description"
            type="text"
            fullWidth
            onChange={e => {
              this.setState({ description: e.target.value });
            }}
          />
          <TextField
            margin="dense"
            label="Address"
            type="text"
            fullWidth
            onChange={e => {
              this.setState({ address: e.target.value });
            }}
          />
          <FormControl className={FormControl} fullWidth>
            <InputLabel htmlFor="demo-controlled-open-select">
              Gender
            </InputLabel>
            <Select
              value={this.state.gender}
              onChange={e => {
                this.setState({ gender: e.target.value });
              }}
              inputProps={{
                name: "gender",
                id: "demo-controlled-open-select"
              }}
            >
              <MenuItem value="Male">Male</MenuItem>
              <MenuItem value="Female">Female</MenuItem>
              <MenuItem value="Other">Other</MenuItem>
            </Select>
          </FormControl>
          <TextField
            margin="dense"
            label="Username"
            type="text"
            fullWidth
            onChange={e => {
              this.setState({ username: e.target.value });
            }}
          />
          <TextField
            margin="dense"
            label="Email"
            type="text"
            fullWidth
            onChange={e => {
              this.setState({ email: e.target.value });
            }}
          />
          <TextField
            margin="dense"
            label="Password"
            type="password"
            fullWidth
            onChange={e => {
              this.setState({ password: e.target.value });
            }}
          />
          <TextField
            margin="dense"
            label="Retype Password"
            type="password"
            fullWidth
            onChange={e => {
              this.setState({ retype_password: e.target.value });
            }}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.closeDialog} color="primary">
            Cancel
          </Button>
          <Button onClick={this.Register} color="secondary">
            Submit
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default Register;
