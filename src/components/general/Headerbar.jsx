import React from 'react';
import '../../css/general/headerbar.css';
import {withRouter} from 'react-router-dom';
import {withGlobalContext} from '../contexts/GlobalContext';
import Login from '../profile/login';
import Register from '../profile/register';
import {confirmAlert} from 'react-confirm-alert';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openLogin: false,
      openRegister: false,
    };
  }
  closeLogin = () => {
    this.setState({openLogin: false});
  };
  closeRegister = () => {
    this.setState({openRegister: false});
  };
  createPost = () => {
    if(window.location.pathname == '/'){
      this.props.history.push('/create-post');
    } 
    else{
      this.props.history.push('/create-post-group');
    }
  };
  createGroup = () => {
    this.props.history.push('/create-group');
  };
  Login = () => {
    //this.props.history.push("/login");
    this.setState({openLogin: true});
  };
  Logout = () => {
    confirmAlert({
      title: 'Confirm to logout',
      message: 'Are you sure to logout?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {
            this.props.global.logout();
            this.props.history.push('/');
          },
        },
        {
          label: 'No',
        },
      ],
    });
  };
  Register = () => {
    //this.props.history.push("/register");
    this.setState({openRegister: true});
  };

  render() {
    //const {onClick} = this.props;
    return (
      <div className="header">
        <div className="headerButtonArea">
          <a href="/" className="headerLink">
            <p className="headerTitle">TECHZONE</p>
          </a>
        </div>
        <input type="text" className="headerSearch" placeholder="Search" />
        {this.props.global.user.id ? (
          <div className="headerButtonArea">
            <button className="headerButtons" onClick={this.createPost}>
              Create Post
            </button>
            <button className="headerButtons" onClick={this.createGroup}>
              Create Group
            </button>
            <button className="headerButtons logout" onClick={this.Logout}>
              Logout
            </button>
          </div>
        ) : (
          <div className="headerButtonArea">
            <button className="headerButtons login" onClick={this.Login}>
              Login
            </button>
            <button className="headerButtons" onClick={this.Register}>
              Register
            </button>
          </div>
        )}
        <Login
          open={this.state.openLogin}
          closeDialog={this.closeLogin}
          saveAccessToken={this.props.global.setAccessToken}
        />
        <Register
          open={this.state.openRegister}
          closeDialog={this.closeRegister}
        />
      </div>
    );
  }
}

export default withGlobalContext(withRouter(Header));
