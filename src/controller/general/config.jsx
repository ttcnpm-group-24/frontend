const api_url_dev = "http://localhost:5000/api";

const api_url_prod = "https://ttcnpm-backend.herokuapp.com/api";

const server_url = "https://ttcnpm-backend.herokuapp.com"

const role = {
  normal: 0,
  moderator: 1,
  owner: 2
};

export { api_url_dev, api_url_prod, server_url };
export default role;
