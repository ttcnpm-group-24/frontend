import React from "react";
import "../../css/group/my-group.css";
import GroupScreenWrapper from "../../components/group/GroupScreenWrapper";
import { withGlobalContext } from "../../components/contexts/GlobalContext";
import GroupInfo from "../../components/group/GroupInfo";
import GroupAdvancedSetting from "../../components/group/GroupAdvancedSetting";
import GroupMemberApi from "../../controller/group/GroupMemberApi";
import GroupOwnerApi from "../../controller/group/GroupOwnerApi";
import GroupApi from "../../controller/group/GroupApi";
import { confirmAlert } from "react-confirm-alert";
import GroupListItem from "../../components/group/GroupListItem";
import GroupModApi from "../../controller/group/GroupModApi";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import GroupAvatar from "../../components/group/GroupAvatar";

class GroupSettingScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      open: false,
      email: null
    };
  }
  componentWillMount() {
    this.props.global.updateCurrentGroup(this.state.id);
  };
  openDialog = () => {
    this.setState({ open: true });
  };
  closeDialog = () => {
    this.setState({ open: false });
  };
  render() {
    return (
      <GroupScreenWrapper
        id={this.state.id}
        isAdmin={this.props.global.isAdmin}
        name={this.props.global.current_group.name}
        ava={this.props.global.current_group.avatar}
        des={this.props.global.current_group.description}
        content={
          <div className="my-group-wrapper">
            <div className="my-group">
              {this.props.global.isOwner ? <GroupAvatar /> : null}
              <GroupInfo
                rules={this.props.global.current_group.rules}
                des={this.props.global.current_group.description}
                isAdmin={this.props.global.isAdmin}
              />
              {this.props.global.isAdmin ? (
                this.props.global.isOwner ? (
                  <GroupAdvancedSetting
                    items={[
                      {
                        name: "Delete",
                        des: "This will delete this group permanently.",
                        normal: false,
                        function: this.deleteGroup
                      },
                      {
                        name: "Change owner",
                        des:
                          "This will change the owner of this group permanently.",
                        normal: true,
                        function: this.openDialog
                      }
                    ]}
                  />
                ) : (
                  <GroupAdvancedSetting
                    items={[
                      {
                        name: "Leave",
                        des: "Leave this group permanently.",
                        normal: false,
                        function: this.leaveGroup
                      },
                      {
                        name: "Become normal user",
                        des:
                          "This will remove all your permission as a moderator",
                        normal: true,
                        function: this.changeToNormal
                      }
                    ]}
                  />
                )
              ) : (
                <GroupAdvancedSetting
                  items={[
                    {
                      name: "Leave",
                      des: "Leave this group permanently.",
                      normal: false,
                      function: this.leaveGroup
                    }
                  ]}
                />
              )}
            </div>
            <Dialog
              open={this.state.open}
              onClose={this.closeDialog}
              aria-labelledby="form-dialog-title"
            >
              <DialogTitle id="form-dialog-title">
                Change owner of {this.props.global.current_group.name}
              </DialogTitle>
              <DialogContent>
                <DialogContentText>
                  To change the owner of {this.props.global.current_group.name},
                  enter a member's email here.
                </DialogContentText>
                <TextField
                  autoFocus
                  margin="dense"
                  label="Email Address"
                  type="email"
                  fullWidth
                  onChange={e => {
                    this.setState({ email: e.target.value });
                  }}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={this.closeDialog} color="primary">
                  Cancel
                </Button>
                <Button onClick={this.changeOwner} color="secondary">
                  Submit
                </Button>
              </DialogActions>
            </Dialog>
          </div>
        }
      />
    );
  }
  leaveGroup = () => {
    confirmAlert({
      title: "Confirm to leave",
      message: "Are you sure to leave to group?",
      buttons: [
        {
          label: "Yes",
          onClick: () => {
            GroupMemberApi.deleteMember(
              {
                user_id: this.props.global.user.id,
                group_id: this.state.id
              },
              this.props.global.access_token
            )
              .then(response => {
                console.log(response);
                this.props.history.push("/");
              })
              .catch(e => {
                console.error(e);
              });
          }
        },
        {
          label: "No"
        }
      ]
    });
  };
  deleteGroup = () => {
    confirmAlert({
      title: "Confirm to delete",
      message:
        "Are you sure to delete " + this.props.global.current_group.name + "?",
      buttons: [
        {
          label: "Yes",
          onClick: () => {
            GroupApi.deleteAGroup(this.state.id, this.props.global.access_token)
              .then(response => {
                console.log(response);
                this.props.history.push("/");
              })
              .catch(e => {
                console.error(e);
              });
          }
        },
        {
          label: "No"
        }
      ]
    });
  };
  changeToNormal = () => {
    confirmAlert({
      title: "Confirm to become normal user",
      message: "Are you sure to change to normal user?",
      buttons: [
        {
          label: "Yes",
          onClick: () => {
            GroupModApi.setModeratorToMember(
              {
                user_id: this.props.global.user.id,
                group_id: this.props.global.group_id
              },
              this.props.global.access_token
            ).then(res => {
              if (res.error) {
                alert("Unable to become normal user");
              } else {
                this.props.global.getMember(this.props.global.user.id);
                this.props.history.push(
                  "/groups/" + this.props.global.current_group.id
                );
              }
            });
          }
        },
        {
          label: "No"
        }
      ]
    });
  };
  changeOwner = () => {
    if (this.state.email && this.state.email !== "") {
      GroupMemberApi.findMember(
        {
          email: this.state.email,
          group_id: this.props.global.current_group.id
        },
        this.props.global.access_token
      )
        .then(res => {
          if (res.length === 0) {
            alert("Member not found");
            return;
          } else {
            GroupOwnerApi.setOwner({
              owner_id: this.props.global.user.id,
              user_id: res[0].id,
              group_id: this.props.global.current_group.id
            })
              .then(res2 => {
                if (res2.error) {
                  alert("Unable to change owner");
                } else {
                  this.props.global.getMember(this.props.global.user.id);
                  this.props.history.push(
                    "/groups/" + this.props.current_group.id
                  );
                }
              })
              .catch(e => {
                console.error(e);
              });
          }
        })
        .catch(e => {
          console.error(e);
        });
      this.closeDialog();
    } else {
      alert("Please enter email address");
    }
  };
}

export default withGlobalContext(GroupSettingScreen);
