import makeRequest from '../general/api';
import role from '../general/config';
import {makeFormDataRequest} from '../general/api';

export const url_wrapper = (url, access_token) => {
  if (access_token) {
    if (url.includes('?')) {
      url = url + '&access_token=' + access_token;
    } else {
      url = url + '?access_token=' + access_token;
    }
  }
  return url;
};

class ProfileApi {
  static Register = data => {
    /**
     * name
     * description
     * address
     * gender
     * username
     * email
     * password
     */
    data.emailVerified = true;
    return makeRequest('/Clients', {
      method: 'POST',
      body: JSON.stringify(data),
    });
  };
  static Login = data => {
    /**
     * username/email
     * password
     */
    return makeRequest('/Clients/login', {
      method: 'POST',
      body: JSON.stringify(data),
    });
  };
  static getUser = (id, access_token) => {
    return makeRequest(url_wrapper('/Clients/' + id, access_token), {
      method: 'GET',
    }).then(res => {
      return makeRequest(
        url_wrapper('/Clients/' + id + '/avatars', access_token),
        {
          method: 'GET',
        },
      ).then(data => {
        res.avatar = data.url;
        console.log(res)
        return res;
      });
    });
  };
  static changePassword = (data, access_token) => {
    /*
     * oldPassword
     * newPassword
     */
    return makeRequest(url_wrapper('/Clients/change-password', access_token), {
      method: 'POST',
      body: JSON.stringify(data),
    });
  };
  static changeProfile = (data, access_token) => {
    /*
     * name
     * description
     * address
     * id
     * email
     */
    let id = data.id;
    delete data['id'];
    return makeRequest(url_wrapper('/Clients/' + id, access_token), {
      method: 'PATCH',
      body: JSON.stringify(data),
    });
  };
  static changeAvatar = (data, access_token) => {
    /*
     * client_id
     * avatar
     */
    // Delete avatar
    return makeRequest(
      url_wrapper('/Clients/' + data.client_id + '/avatars', access_token),
      {
        method: 'DELETE',
      },
    ).then(count => {
      let formData = new FormData();
      formData.append('avatar', data.avatar);
      return makeFormDataRequest(
        url_wrapper(
          '/Avatars/uploadClient?client_id=' + data.client_id,
          access_token,
        ),
        {
          method: 'POST',
          body: formData,
        },
      );
    });
  };
}

export default ProfileApi;
