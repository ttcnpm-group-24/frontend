import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import ProfileApi from '../../controller/profile/ProfileApi';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user_email: '',
      password: '',
    };
  }
  Login = () => {
    let data = {};
    if (this.state.user_email.includes('@')) {
      data.email = this.state.user_email;
    } else {
      data.username = this.state.user_email;
    }
    data.password = this.state.password;
    ProfileApi.Login(data)
      .then(res => {
        if (res.error) {
          alert('Wrong username/email or password');
        } else {
          this.props.saveAccessToken(res.id, res.userId);
        }
      })
      .catch(e => {
        console.error(e);
      })
      .then(() => {
        this.props.closeDialog();
      });
  };
  render() {
    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.closeDialog}
        aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Login</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Enter username/email and password to login.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            label="Username/Email"
            type="text"
            fullWidth
            onChange={e => {
              this.setState({user_email: e.target.value});
            }}
          />
          <TextField
            margin="dense"
            label="Password"
            type="password"
            fullWidth
            onChange={e => {
              this.setState({password: e.target.value});
            }}
            onKeyPress={e => {
              if (e.key === 'Enter') {
                this.Login();
              }
            }}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.closeDialog} color="primary">
            Cancel
          </Button>
          <Button onClick={this.Login} color="secondary">
            Submit
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default Login;
