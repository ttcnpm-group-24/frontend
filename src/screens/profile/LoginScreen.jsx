import React from "react";
import { withGlobalContext } from "../../components/contexts/GlobalContext";
import { withRouter,Link } from "react-router-dom";
import ScreenWrapper from "../../components/general/ScreenWrapper";
import "../../css/profile/loginscreen.css";

class LoginScreen extends React.Component {
      Forget = () => {
        this.props.history.push("/forget");
      };
      constructor(props) {
        super(props);
        this.state = {
          username: ""
        };
      }
      cancel = () => {
        this.props.history.push("/");
      };
      getInput = username => {
        if (username === "username") {
          return this.state.username;
        }
      };
      renderContent = () => {
        return (
          <div className="form-login">
            <div className="dialog">
              <div className="info">
                <div className="content">
                  <h1>Login</h1>
                </div>
                <div className="content">
                  <div className="title">
                    <h4>Username</h4>
                  </div>
                  <input
                    type="text"
                    name="username"
                    placeholder="Enter your username"
                  />
                </div>
                <div className="content">
                  <div className="title">
                    <h4>Password</h4>
                  </div>
                  <input
                    type="password"
                    name="password"
                    placeholder="Enter your password"
                  />
                </div>
                <div className="content" id="buttons">
                  <button className="cancel" onClick={this.cancel}>
                    Cancel
                  </button>
                  <button className="submit">Login</button>
                </div>
                {/* <div className="forget">
                  <div onClick={this.Forget}>
                     Forget password
                  </div>
                </div> */}

                <div className="forget">
                  <Link to="/forget">Forget password</Link>
                </div>
                
              </div>
            </div>
          </div>
        );
      };
      render() {
        return (
          <ScreenWrapper
            sidebar_name={this.props.global.user.name}
            sidebar_ava={this.props.global.user.ava}
            sidebar_des={this.props.global.user.des}
            sidebar_buttons={this.props.global.getButtons()}
            content={this.renderContent()}
          />
        );
      }
    }

export default withGlobalContext(withRouter(LoginScreen));
