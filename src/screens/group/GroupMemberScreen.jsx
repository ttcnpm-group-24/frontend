import React from 'react';
import {withGlobalContext} from '../../components/contexts/GlobalContext';
import '../../css/group/my-group.css';
import GroupListItem from '../../components/group/GroupListItem';
import GroupScreenWrapper from '../../components/group/GroupScreenWrapper';
import logo from '../../logo.svg';
import GroupMemberApi from '../../controller/group/GroupMemberApi';
import GroupApi from '../../controller/group/GroupApi';
import role from '../../controller/general/config';
import GroupOwnerApi from '../../controller/group/GroupOwnerApi';
import {confirmAlert} from 'react-confirm-alert';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Loading from '../../components/general/Loading';

class GroupMemberScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      data: [],
      open: false,
      email_addr: null,
      loading: true,
    };
  }
  getMember() {
    return GroupMemberApi.getMember(
      this.state.id,
      this.props.global.access_token,
    )
      .then(res => {
        this.setState({data: res});
        console.log(res);
      })
      .catch(e => {
        console.error(e);
      });
  }
  componentWillMount() {
    this.props.global.updateCurrentGroup(this.state.id);
    this.getMember().then(() => {
      this.setState({loading: false});
    });
  }
  renderAdminContent(item, index) {
    return (
      <GroupListItem
        key={index}
        item={item}
        menu={[
          {
            name: 'Remove from group',
            function: () => {
              this.removeSelectedUser(item.id);
            },
          },
          {
            name: 'Profile',
            function: () => {
              this.removeSelectedUser(item.id);
            },
          },
        ]}
      />
    );
  }
  renderUserContent(item, index) {
    return (
      <GroupListItem
        key={index}
        item={item}
        menu={[
          {
            name: 'Profile',
            function: () => {
              this.props.history.push(item.link);
            },
          },
        ]}
      />
    );
  }
  renderOwnerContent(item, index) {
    return (
      <GroupListItem
        key={index}
        item={item}
        menu={[
          {
            name: 'Remove from group',
            function: () => {
              this.removeSelectedUser(item.id);
            },
          },
          {
            name: 'Change to owner',
            function: () => {
              this.makeUserOwner(item.id);
            },
          },
          {
            name: 'Profile',
            function: () => {
              this.props.history.push(item.link);
            },
          },
        ]}
      />
    );
  }
  renderContent(item, index) {
    if (this.props.global.isAdmin) {
      if (this.props.global.isOwner) {
        return this.renderOwnerContent(item, index);
      } else {
        return this.renderAdminContent(item, index);
      }
    } else {
      return this.renderUserContent(item, index);
    }
  }
  openDialog = () => {
    this.setState({open: true});
  };
  closeDialog = () => {
    this.setState({open: false});
  };
  addMember = () => {
    // Find User
    if (this.state.email && this.state.email !== '') {
      this.setState({loading: true});
      GroupMemberApi.findUser(
        this.state.email,
        this.props.global.access_token,
      ).then(res => {
        if (res.length === 0) {
          alert('User not found');
          return;
        } else {
          GroupMemberApi.addMember(
            {
              user_id: res[0].id,
              group_id: this.props.global.current_group.id,
            },
            this.props.global.access_token,
          ).then(res2 => {
            if (res2.error) {
              alert('Member already existed');
              return;
            } else {
              this.getMember().then(() => {
                this.setState({loading: false});
              });
            }
          });
        }
      });
      this.closeDialog();
    } else {
      alert('Please enter email address');
      return;
    }
  };
  render() {
    return (
      <GroupScreenWrapper
        id={this.state.id}
        name={this.props.global.current_group.name}
        isAdmin={this.props.global.isAdmin}
        ava={this.props.global.current_group.avatar}
        des={this.props.global.current_group.description}
        content={
          this.state.loading ? (
            <Loading />
          ) : (
            <div className="my-group-wrapper">
              <div className="title">
                <h2>Members of {this.props.global.current_group.name}</h2>
                <button onClick={this.openDialog}>Add member</button>
              </div>
              <div className="my-group">
                {this.state.data.map((item, index) => {
                  item.avatar = logo;
                  item.link = '/clients/' + item.id;
                  return this.renderContent(item, index);
                })}
              </div>
              <Dialog
                open={this.state.open}
                onClose={this.closeDialog}
                aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Add member</DialogTitle>
                <DialogContent>
                  <DialogContentText>
                    To make a user a member of{' '}
                    {this.props.global.current_group.name}, enter that user's
                    email here.
                  </DialogContentText>
                  <TextField
                    autoFocus
                    margin="dense"
                    label="Email Address"
                    type="text"
                    fullWidth
                    onChange={e => {
                      this.setState({email: e.target.value});
                    }}
                  />
                </DialogContent>
                <DialogActions>
                  <Button onClick={this.closeDialog} color="primary">
                    Cancel
                  </Button>
                  <Button onClick={this.addMember} color="secondary">
                    Submit
                  </Button>
                </DialogActions>
              </Dialog>
            </div>
          )
        }
      />
    );
  }
  removeSelectedUser(user_id) {
    /* Admin cannot remove other admin
     * Only owner can remove other admin
     */
    // Check admin
    if (user_id === this.props.global.user.id) {
      alert('This is you');
      return;
    } else {
      confirmAlert({
        title: 'Confirm to remove',
        message: 'Are you sure to this user from group?',
        buttons: [
          {
            label: 'Yes',
            onClick: () => {
              if (this.props.global.isOwner) {
                this.setState({loading: true});
                GroupMemberApi.deleteMember(
                  {
                    user_id: user_id,
                    group_id: this.props.global.current_group.id,
                  },
                  this.props.global.access_token,
                )
                  .then(res => {
                    if (res.error) {
                      alert(
                        'Unable to remove this user from ' +
                          this.props.global.current_group.name,
                      );
                    } else {
                      this.getMember().then(() => {
                        this.setState({loading: false});
                      });
                    }
                  })
                  .catch(e => {
                    console.error(e);
                  });
              } else if (this.props.global.isAdmin) {
                GroupApi.getRoleInGroup(
                  {
                    user_id: user_id,
                    group_id: this.props.global.current_group.id,
                  },
                  this.props.global.access_token,
                ).then(r => {
                  if (r === role.moderator || r === role.owner) {
                    alert('You cannot remove this user');
                    return;
                  } else {
                    this.setState({loading: true});
                    GroupMemberApi.deleteUser(
                      {
                        user_id: user_id,
                        group_id: this.props.global.current_group.id,
                      },
                      this.props.global.access_token,
                    )
                      .then(res => {
                        if (res.error) {
                          alert(
                            'Unable to remove this user from ' +
                              this.props.global.current_group.name,
                          );
                        } else {
                          this.getMember().then(() => {
                            this.setState({loading: false});
                          });
                        }
                      })
                      .catch(e => {
                        console.error(e);
                      });
                  }
                });
              }
            },
          },
          {
            label: 'No',
          },
        ],
      });
    }
  }
  makeUserOwner(user_id) {
    if (user_id === this.props.global.user.id) {
      alert('This is you');
      return;
    } else {
      confirmAlert({
        title: 'Confirm to change owner',
        message: 'Are you sure to make this user owner?',
        buttons: [
          {
            label: 'Yes',
            onClick: () => {
              this.setState({loading: true});
              GroupOwnerApi.setOwner(
                {
                  owner_id: this.props.global.user.id,
                  user_id: user_id,
                  group_id: this.props.global.current_group.id,
                },
                this.props.global.access_token,
              )
                .then(res => {
                  if (res.error) {
                    alert('Unable to change owner');
                    return;
                  } else {
                    this.props.global.getGroup(this.state.id).then(() => {
                      this.getMember().then(() => {
                        this.setState({loading: false});
                      });
                    });
                  }
                })
                .catch(e => {
                  console.error(e);
                });
            },
          },
          {
            label: 'No',
          },
        ],
      });
    }
  }
}

export default withGlobalContext(GroupMemberScreen);
