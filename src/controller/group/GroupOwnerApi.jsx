import makeRequest from '../general/api';
import role from '../general/config';
import {
  url_wrapper,
  addToGroup,
  removeUserFromGroup,
  changeRoleInGroup,
} from './GroupApi';
import {access} from 'fs';

export default class GroupOwnerApi {
  static setOwner = (data, access_token) => {
    /**
     * owner_id
     * user_id
     * group_id
     */
    return changeRoleInGroup(
      {
        role: role.owner,
        user_id: data.user_id,
        group_id: data.group_id,
      },
      access_token,
    )
      .then(response => {
        if (response.error) {
          alert('Unable to set owner');
          return;
        } else {
          return changeRoleInGroup({
            role: role.moderator,
            user_id: data.owner_id,
            group_id: data.group_id,
          });
        }
      })
      .catch(e => {
        console.error(e);
      });
  };
}
