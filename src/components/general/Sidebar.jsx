import React from 'react';
import '../../css/general/sidebar.css';
import { withRouter } from 'react-router-dom';
import logo from '../../logo.svg';
import { server_url } from '../../controller/general/config';

class Sidebar extends React.Component {
  goto = link => {
    // Navigate to other screens
    this.props.history.push(link);
  };
  render() {
    return (
      <div className="sidebar">
        <div className="sidebar-content">
          <img
            className="sidebarAvatar"
            src={
              this.props.avatar && this.props.avatar !== ''
                ? server_url + this.props.avatar
                : logo
            }
            alt="Avatar"
          />
          <h1 className="sidebarName">
            {this.props.name ? this.props.name : 'Welcome'}
          </h1>
          <h3 className="sidebarDescription">
            {this.props.description
              ? this.props.description
              : 'Knowledge is power'}
          </h3>
          {this.props.buttons.map((item, index) => {
            return (
              <button
                className="sidebarButton"
                key={index}
                onClick={() => {
                  if (item.function) {
                    item.function();
                  } else if (item.link) {
                    this.goto(item.link);
                  }
                }}>
                {item.name}
              </button>
            );
          })}
        </div>
      </div>
    );
  }
}

export default withRouter(Sidebar);
