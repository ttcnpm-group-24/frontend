import React from "react";
import "../../css/post/addpost.css";
import ScreenWrapper from "../../components/general/ScreenWrapper";
import { withGlobalContext } from "../../components/contexts/GlobalContext";
import { withRouter } from "react-router-dom";
import PostApi from "../../controller/post/PostApi";
import logo from "../../logo.svg";

class CreatePostScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "",
      content: "",
      des: "",
      upvote: 0,
      image: {},
      time: "", 
      group_id: null,
      ava: logo
    }
  }
  createPost = () => {
    if(window.location.pathname == '/create-post-group'){
      var gr_id = this.props.global.current_group.id;
    }
    var today = new Date();
    PostApi.createPost(
      { 
        user_id: this.props.global.user.id,
        title: this.state.title,
        content: this.state.content,
        upvote: this.state.upvote,
        image: this.state.image,
        time: today,
        group_id: gr_id
      },
      this.props.global.access_token
    )
      .then(data => {
        alert(data);
        //alert(data.time)
        this.props.history.push("/");
        
      })
      .catch(e => {
        console.error(e);
      });
    }
    addPost = () => {
      this.createPost();
    }
    cancel = () => {
      this.props.history.push("/");
    };
    renderContent = () => {
      
      return (
        
        <div className="create-post">
            <div>
              <h1>CreatePost</h1>
            </div>
            <div className="title">
              <textarea className="title-area"
              name="editor" 
              onChange={e => {this.setState({title: e.target.value});}} 
              placeholder="Enter the title here" maxLength="30">
              </textarea>
            </div>
            <div className="cont">
              <textarea className="cont-area" 
               name="editor1"
              onChange={e => {this.setState({content: e.target.value});}} 
               placeholder="Enter the content here"
               rows="5" cols="10">
              </textarea>
            </div>
            <div className="buttons">
              <button className="cancel" type="button" onClick={this.cancel}>Cancel</button>
              <button className="submit" type="button" onClick={this.addPost} id="submit" name="submit">Add Post</button>
              
            </div>
        </div>
      )
    }

    render(){
      return(
        <ScreenWrapper
          sidebar_name={this.props.global.user.name}
          sidebar_ava={this.props.global.user.avatar}
          sidebar_des={this.props.global.user.description}
          sidebar_buttons={this.props.global.getButtons()}
          content={this.renderContent()}
        />
      )
    }
}
export default withGlobalContext(withRouter(CreatePostScreen));