import makeRequest from '../general/api';
import role from '../general/config';
import {makeFormDataRequest} from '../general/api';

/*
    GET and DELETE method doesn't have body,
    instead data is passed as parameter on url
*/

export const url_wrapper = (url, access_token) => {
  if (access_token) {
    if (url.includes('?')) {
      url = url + '&access_token=' + access_token;
    } else {
      url = url + '?access_token=' + access_token;
    }
  }
  return url;
};

export const addToGroup = (data, access_token) => {
  /**
   * user_id
   * group_id
   * role
   */
  return makeRequest(
    // Check existing member
    url_wrapper(
      '/Groups/' + data.group_id + '/users/rel/' + data.user_id,
      access_token,
    ),
    {
      method: 'HEAD',
    },
  )
    .then(response => {
      if (response === 200) {
        // Member existed
        return new Promise((resolve, reject) => {
          resolve({
            error: {
              code: 404,
              message: 'Member already existed',
            },
          });
        });
      } else {
        // Member not existed
        return makeRequest(url_wrapper('/GroupMembers', access_token), {
          method: 'POST',
          body: JSON.stringify({
            role: data.role,
            user_id: data.user_id,
            group_id: data.group_id,
          }),
        });
      }
    })
    .catch(e => {
      console.error(e);
    });
};

export const removeUserFromGroup = (data, access_token) => {
  return makeRequest(
    url_wrapper(
      '/Groups/' + data.group_id + '/users/rel/' + data.user_id,
      access_token,
    ),
    {
      method: 'DELETE',
    },
  );
};

export const changeRoleInGroup = (data, access_token) => {
  /**
   * role
   * user_id
   * group_id
   */
  const filter = JSON.stringify({
    where: {
      user_id: data.user_id,
      group_id: data.group_id,
    },
  });
  return makeRequest(
    url_wrapper('/GroupMembers?filter=' + filter, access_token),
    {
      method: 'GET',
    },
  )
    .then(response => {
      return makeRequest(url_wrapper('/GroupMembers', access_token), {
        method: 'PUT',
        body: JSON.stringify({
          role: data.role,
          id: response[0].id,
          user_id: response[0].user_id,
          group_id: response[0].group_id,
        }),
      });
    })
    .catch(e => {
      console.error(e);
    });
};

class GroupApi {
  static createGroup = (data, access_token) => {
    /**
     * user_id
     * name
     * description
     * avatar
     * rules
     */
    // create a plain group
    const user_id = data.user_id;
    let formData = new FormData();
    formData.append('avatar', data.avatar);
    delete data['user_id'];
    delete data['avatar'];
    return makeRequest(url_wrapper('/Groups', access_token), {
      method: 'POST',
      body: JSON.stringify(data),
    })
      .then(response => {
        console.log(response);
        // create relationship from member to group as owner
        return makeRequest(url_wrapper('/GroupMembers', access_token), {
          method: 'POST',
          body: JSON.stringify({
            role: role.owner,
            user_id: user_id,
            group_id: response.id,
          }),
        })
          .then(res => {
            if (data.avatar) {
              return makeFormDataRequest(
                url_wrapper(
                  '/Avatars/uploadGroup?group_id=' + response.id,
                  access_token,
                ),
                {
                  method: 'POST',
                  body: formData,
                },
              );
            } else {
              return res;
            }
          })
          .catch(e => {
            console.error(e);
          });
      })
      .catch(e => {
        console.error(e);
      });
  };

  static getMyGroup = (id, access_token) => {
    return makeRequest(
      url_wrapper('/Clients/' + id + '/groups', access_token),
      {
        method: 'GET',
      },
    );
  };

  static getAGroup = (id, access_token) => {
    return makeRequest(url_wrapper('/Groups/' + id, access_token), {
      method: 'GET',
    })
      .then(res => {
        return makeRequest(
          url_wrapper('/Groups/' + id + '/avatars', access_token),
          {
            method: 'GET',
          },
        )
          .then(ava => {
            console.log(ava);
            if (ava.error) {
              console.error('Error getting avatar');
            } else {
              res.avatar = ava.url;
            }
            return res;
          })
          .catch(e => {
            console.error(e);
          });
      })
      .catch(e => {
        console.error(e);
      });
  };

  static getAvatar = (group_id, access_token) => {
    return makeRequest(
      url_wrapper('/Groups/' + group_id + '/avatars', access_token),
      {
        method: 'GET',
      },
    );
  };

  static updateAGroup = (data, access_token) => {
    /**
     * id
     * name
     * description
     * rules
     */
    return makeRequest(url_wrapper('/Groups/' + data.id, access_token), {
      method: 'PUT',
      body: JSON.stringify(data),
    });
  };

  static updateAvatar = (data, access_token) => {
    /**
     * group_id
     * avatar
     */
    // Delete avatar
    return makeRequest(
      url_wrapper('/Groups/' + data.group_id + '/avatars', access_token),
      {
        method: 'DELETE',
      },
    ).then(count => {
      const formData = new FormData();
      formData.append('avatar', data.avatar);
      return makeFormDataRequest(
        url_wrapper(
          '/Avatars/uploadGroup?group_id=' + data.group_id,
          access_token,
        ),
        {
          method: 'POST',
          body: formData,
        },
      );
    });
  };
  static getAllGroup = access_token => {
    return makeRequest(url_wrapper('/Groups', access_token), {
      method: 'GET',
    });
  };

  static deleteAGroup = (id, access_token) => {
    return makeRequest(url_wrapper('/Groups/' + id, access_token), {
      method: 'DELETE',
    });
  };

  static getGroupPosts = (id, access_token) => {
    return makeRequest(url_wrapper('/Groups/' + id + '/posts', access_token), {
      method: 'GET',
    });
  };

  static getRoleInGroup = (data, access_token) => {
    const filter = JSON.stringify({
      where: {
        user_id: data.user_id,
        group_id: data.group_id,
      },
    });
    return makeRequest(
      url_wrapper('/GroupMembers?filter=' + filter, access_token),
      {
        method: 'GET',
      },
    )
      .then(response => {
        return response[0].role;
      })
      .catch(e => {
        console.error(e);
      });
  };
}

export default GroupApi;
