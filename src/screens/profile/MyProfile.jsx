import React from 'react';
import ScreenWrapper from '../../components/general/ScreenWrapper';
import {withGlobalContext} from '../../components/contexts/GlobalContext';
import '../../css/profile/profile.css';
import ProfileApi from '../../controller/profile/ProfileApi';
import {confirmAlert} from 'react-confirm-alert';
import Loading from '../../components/general/Loading';
import {server_url} from '../../controller/general/config';
import logo from '../../logo.svg';

const Compress = require('compress.js');
const compress = new Compress();

class MyProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.global.user.id,
      name: this.props.global.user.name,
      description: this.props.global.user.description,
      address: this.props.global.user.address,
      gender: this.props.global.user.gender,
      email: this.props.global.user.email,
      username: this.props.global.user.username,
      avatar_display: this.props.global.user.avatar
        ? server_url + this.props.global.user.avatar
        : logo,
      avatar: null,
      edit: false,
    };
  }
  cancel = () => {
    this.setState({edit: false});
  };
  submit = () => {
    confirmAlert({
      title: 'Confirm to change',
      message: 'Are you sure to change your password?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {
            this.setState({loading: true});
            ProfileApi.changeAvatar(
              {
                client_id: this.state.id,
                avatar: this.state.avatar,
              },
              this.props.global.access_token,
            ).then(res => {
              let user_data = this.props.global.user;
              user_data.name = this.state.name;
              user_data.description = this.state.description;
              user_data.address = this.state.address;
              ProfileApi.changeProfile(
                user_data,
                this.props.global.access_token,
              ).then(() => {
                ProfileApi.getUser(
                  this.state.id,
                  this.props.global.access_token,
                )
                  .then(ret => {
                    this.props.global.setUser(ret);
                  })
                  .then(() => {
                    this.setState({loading: false, edit: false});
                  });
              });
            });
          },
        },
        {
          label: 'No',
        },
      ],
    });
  };
  renderContent = () => {
    return (
      <div className="edit-profile">
        <h2 className="page-title">Profile of {this.state.name}</h2>
        <div className="avatar">
          <img src={this.state.avatar_display} alt="ava" />
          <div className="img-selection">
            <h4>Avatar</h4>
            {this.state.edit ? (
              <p>Set your profile picture by uploading an image</p>
            ) : (
              <p>This is the avatar of {this.state.name}</p>
            )}
            {this.state.edit ? (
              <input
                type="file"
                name="ava"
                accept="image/x-png,image/jpeg"
                onChange={e => {
                  if (e.target.files && e.target.files.length) {
                    const images = [...e.target.files];
                    compress
                      .compress(images, {
                        size: 1,
                        quality: 1,
                      })
                      .then(data => {
                        const img = data[0];
                        const base64str = img.data;
                        const imgExt = img.ext;
                        const file = Compress.convertBase64ToFile(
                          base64str,
                          imgExt,
                        );
                        this.setState({
                          avatar_display: URL.createObjectURL(file),
                          avatar: file,
                        });
                      });
                  }
                }}
              />
            ) : null}
          </div>
        </div>
        <h4 className="page-title">Name</h4>
        <div className="input">
          <input
            type="text"
            name="name"
            placeholder="Enter your name here"
            value={this.state.name}
            disabled={!this.state.edit}
            onChange={e => {
              this.setState({name: e.target.value});
            }}
          />
        </div>
        <h4 className="page-title">Description</h4>
        <div className="input">
          <textarea
            type="text"
            name="group-name"
            placeholder="Enter your description here"
            value={this.state.description}
            disabled={!this.state.edit}
            onChange={e => {
              this.setState({description: e.target.value});
            }}
          />
        </div>
        <h4 className="page-title">Address</h4>
        <div className="input">
          <input
            type="text"
            name="address"
            placeholder="Enter your address here"
            value={this.state.address}
            disabled={!this.state.edit}
            onChange={e => {
              this.setState({address: e.target.value});
            }}
          />
        </div>
        <h4 className="page-title">Email</h4>
        <div className="input">
          <input
            type="text"
            name="email"
            placeholder="Enter your email here"
            value={this.state.email}
            disabled={true}
            onChange={e => {
              this.setState({email: e.target.value});
            }}
          />
        </div>
        <h4 className="page-title">Username</h4>
        <div className="input">
          <input
            type="text"
            name="username"
            placeholder="Enter your username here"
            value={this.state.username}
            disabled={true}
            onChange={e => {
              this.setState({username: e.target.value});
            }}
          />
        </div>
        <div className="buttons">
          {this.state.edit ? (
            <button className="cancel" onClick={this.cancel}>
              Cancel
            </button>
          ) : (
            <button
              className="cancel"
              onClick={() => {
                this.setState({edit: true});
              }}>
              Edit
            </button>
          )}
          <button className="submit" onClick={this.submit}>
            Submit
          </button>
        </div>
      </div>
    );
  };
  render() {
    return (
      <ScreenWrapper
        sidebar_name={this.props.global.user.name}
        sidebar_ava={this.props.global.user.avatar}
        sidebar_des={this.props.global.user.description}
        sidebar_buttons={this.props.global.getButtons()}
        content={this.state.loading ? <Loading /> : this.renderContent()}
      />
    );
  }
}

export default withGlobalContext(MyProfileScreen);
