import React from "react";
import ScreenWrapper from "../../components/general/ScreenWrapper";
import { withGlobalContext } from "../../components/contexts/GlobalContext";
import { withRouter } from "react-router-dom";
import "../../css/group/create-group.css";
import logo from "../../logo.svg";
import GroupApi from "../../controller/group/GroupApi";

const Compress = require("compress.js");
const compress = new Compress();

class CreateGroupScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      group_name: "",
      group_ava_display: logo,
      group_ava: null,
      group_description: "",
      group_rules: ""
    };
  }
  createGroup = () => {
    GroupApi.createGroup(
      {
        user_id: this.props.global.user.id,
        name: this.state.group_name,
        description: this.state.group_description,
        avatar: this.state.group_ava,
        rules: this.state.group_rules
      },
      this.props.global.access_token
    )
      .then(data => {
        console.log(data);
        if (!data.error) {
          this.props.history.push("/groups/home/" + data.group_id);
        } else {
          alert("Something went wrong. Try again later.");
          return;
        }
      })
      .catch(e => {
        console.error(e);
      });
  };
  cancel = () => {
    this.props.history.push("/");
  };
  submit = () => {
    if (this.state.group_name === "") {
      alert("Group's name must be filled");
      return;
    } else {
      this.createGroup();
    }
  };
  getInput = name => {
    if (name === "group-name") {
    } else if (name === "group-des") {
    } else if (name === "group-ava") {
      return this.state.group_ava;
    }
  };
  renderContent = () => {
    return (
      <div className="create-group">
        <h2 className="page-title">Create a group</h2>
        <div className="avatar">
          <img src={this.state.group_ava_display} alt="group-ava" />
          <div className="img-selection">
            <h4>Avatar</h4>
            <p>Set your group's profile picture by uploading an image</p>
            <input
              type="file"
              name="group-ava"
              accept="image/x-png,image/jpeg"
              onChange={e => {
                if (e.target.files && e.target.files.length) {
                  const images = [...e.target.files];
                  compress
                    .compress(images, {
                      size: 1,
                      quality: 1
                    })
                    .then(data => {
                      const img = data[0];
                      const base64str = img.data;
                      const imgExt = img.ext;
                      const file = Compress.convertBase64ToFile(
                        base64str,
                        imgExt
                      );
                      this.setState({
                        group_ava_display: URL.createObjectURL(file),
                        group_ava: file
                      });
                    });
                }
              }}
            />
          </div>
        </div>
        <h4 className="page-title">Name</h4>
        <div className="input">
          <input
            type="text"
            name="group-name"
            placeholder="Enter your group's name here"
            onChange={e => {
              this.setState({ group_name: e.target.value });
            }}
          />
        </div>
        <h4 className="page-title">Description</h4>
        <div className="input">
          <textarea
            type="text"
            name="group-name"
            placeholder="Enter your group's description here"
            onChange={e => {
              this.setState({ group_description: e.target.value });
            }}
          />
        </div>
        <h4 className="page-title">Rules</h4>
        <div className="input">
          <textarea
            type="text"
            name="group-rules"
            placeholder="Enter your group's rules here"
            onChange={e => {
              this.setState({ group_rules: e.target.value });
            }}
          />
        </div>
        <div className="buttons">
          <button className="cancel" onClick={this.cancel}>
            Cancel
          </button>
          <button className="submit" onClick={this.submit}>
            Submit
          </button>
        </div>
      </div>
    );
  };
  render() {
    return (
      <ScreenWrapper
        sidebar_name={this.props.global.user.name}
        sidebar_ava={this.props.global.user.ava}
        sidebar_des={this.props.global.user.des}
        sidebar_buttons={this.props.global.getButtons()}
        content={this.renderContent()}
      />
    );
  }
}

export default withGlobalContext(withRouter(CreateGroupScreen));
