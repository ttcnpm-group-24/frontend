import React, {Component} from 'react';
import HomeScreen from './screens/HomeScreen';
import GroupHomeScreen from './screens/group/GroupHomeScreen';
import CreateGroupScreen from './screens/group/CreateGroupScreen';
import CreatePostScreen from './screens/post/CreatePostScreen';
import Header from './components/general/Headerbar';
import './css/general/page.css';
import {Route, Switch, BrowserRouter as Router} from 'react-router-dom';
import GlobalContextProvider from './components/contexts/GlobalContext';
import MyGroupScreen from './screens/group/MyGroupScreen';
import GroupMemberScreen from './screens/group/GroupMemberScreen';
import GroupSettingScreen from './screens/group/GroupSettingScreen';
import GroupModeratorScreen from './screens/group/GroupModeratorScreen';
import LoginScreen from './screens/profile/LoginScreen';
import RegisterScreen from './screens/profile/RegisterScreen';
import ForgetPasswordScreen from './screens/profile/ForgetPasswordScreen';
import ChangePasswordScreen from './screens/profile/ChangePasswordScreen';
import ShowPost from './screens/post/ShowPost';
import Navigator from './components/general/Navigator';
import ProfileScreen from './screens/profile/Profile';
import MyProfileScreen from './screens/profile/MyProfile';
import PostScreen from './screens/post/PostScreen';
import FindGroup from './screens/group/FindGroup';

const screens = [
  {
    screen: GroupHomeScreen,
    path: '/groups/home/:id',
  },
  {
    screen: CreateGroupScreen,
    path: '/create-group',
  },
  {
    screen: MyGroupScreen,
    path: '/my-groups',
  },
  {
    screen: CreatePostScreen,
    path: '/create-post',
  },
  {
    screen: CreatePostScreen,
    path: '/create-post-group',
  },
  {
    screen: ShowPost,
    path: '/post/:id',
  },
  {
    screen: GroupMemberScreen,
    path: '/groups/members/:id',
  },
  {
    screen: GroupSettingScreen,
    path: '/groups/settings/:id',
  },
  {
    screen: GroupModeratorScreen,
    path: '/groups/moderators/:id',
  },
  {
    screen: LoginScreen,
    path: '/login',
  },
  {
    screen: RegisterScreen,
    path: '/register',
  },
  {
    screen: ForgetPasswordScreen,
    path: '/forget',
  },
  {
    screen: ChangePasswordScreen,
    path: '/change-password/:id',
  },
  {
    screen: ProfileScreen,
    path: '/clients/:id',
  },
  {
    screen: MyProfileScreen,
    path: '/my-profile',
  },
  {
    screen: FindGroup,
    path: '/find-groups',
  },
];

class App extends Component {
  render() {
    return (
      <div id="app">
        <GlobalContextProvider>
          <Router
            ref={navigatorRef => Navigator.setTopLevelNavigator(navigatorRef)}>
            <Header />
            <Switch>
              <Route exact path="/" component={HomeScreen} />
              {screens.map((item, index) => (
                <Route key={index} path={item.path} component={item.screen} />
              ))}
              <Route component={() => <p>Not found</p>} />
            </Switch>
          </Router>
        </GlobalContextProvider>
      </div>
    );
  }
}

export default App;
