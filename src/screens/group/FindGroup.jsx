import React from 'react';
import {withGlobalContext} from '../../components/contexts/GlobalContext';
import '../../css/group/my-group.css';
import ScreenWrapper from '../../components/general/ScreenWrapper';
import GroupListItem from '../../components/group/GroupListItem';
import Loading from '../../components/general/Loading';

class FindGroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }
  componentWillMount = () => {
    this.props.global.getAllGroup().then(() => {
      this.setState({loading: false});
    });
  };
  renderContent = (item, index) => {
    return (
      <GroupListItem
        key={index}
        item={item}
        menu={[
          {
            name: 'Group Profile',
            function: () => {
              this.props.history.push(item.link);
            },
          },
        ]}
      />
    );
  };
  render() {
    return (
      <ScreenWrapper
        sidebar_name={this.props.global.user.name}
        sidebar_ava={this.props.global.user.avatar}
        sidebar_des={this.props.global.user.description}
        sidebar_buttons={this.props.global.getButtons()}
        content={
          this.state.loading ? (
            <Loading />
          ) : (
            <div className="my-group-wrapper">
              <div className="my-group">
                {this.props.global.foundGroup.map((item, index) =>
                  this.renderContent(item, index),
                )}
              </div>
            </div>
          )
        }
      />
    );
  }
}

export default withGlobalContext(FindGroup);
