import React from "react";
import { withGlobalContext } from "../../components/contexts/GlobalContext";
import { withRouter } from "react-router-dom";
import "../../css/post/show-post.css";
import PostApi from "../../controller/post/PostApi";
class ShowPost extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            id: this.props.match.params.id,
            data: {
                title: "",
                // des: "this is des",
                content: ""
            }
        }
    }
    getPost() {
        PostApi.getPost(this.state.id, this.props.global.access_token)
          .then(res => {
            this.setState({ data: res });
            console.log(res);
          })
          .catch(e => {
            console.error(e);
          });
      }
      componentDidMount = () => {
        this.getPost();
      };
    render(){
        return (
            <div className="show-post">
                
                <div className="side"></div>
                <div className="content3">
                    <h3>{this.state.data.title}</h3>
                    
                    <pre>{this.state.data.content}</pre>
                    
                    
                </div>

                <div className="side"></div>
            </div>
        )
    }
}

export default withGlobalContext(withRouter(ShowPost));