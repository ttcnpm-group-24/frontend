import React from "react";
import "../../css/group/setting-group.css";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css"; // Import css
import { withGlobalContext } from "../contexts/GlobalContext";
import GroupApi from "../../controller/group/GroupApi";

class GroupInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      edit: false,
      rules: this.props.rules,
      rule_rows: 1,
      des: this.props.des,
      des_rows: 1
    };
  }
  _save = () => {
    confirmAlert({
      title: "Confirm to save",
      message: "Are you sure to save all changes?",
      buttons: [
        {
          label: "Yes",
          onClick: () => {
            this.setState({ edit: false });
            GroupApi.updateAGroup(
              {
                id: this.props.global.current_group.id,
                name: this.props.global.current_group.name,
                description: this.state.des,
                rules: this.state.rules
              },
              this.props.global.access_token
            )
              .then(data => {
                this.props.global.getGroup(data.id);
              })
              .catch(e => {
                console.error(e);
              });
          }
        },
        {
          label: "No"
        }
      ]
    });
  };
  _cancel = () => {
    confirmAlert({
      title: "Confirm to cancel",
      message: "Are you sure to cancel all changes?",
      buttons: [
        {
          label: "Yes",
          onClick: () => this.setState({ edit: false })
        },
        {
          label: "No"
        }
      ]
    });
  };
  _handleRulesChange = e => {
    //e.target.rows = 1; // reset then update rows
    const currentRows = ~~(e.target.scrollHeight / 24);
    this.setState({ rules: e.target.value, rule_rows: currentRows });
  };
  _handleDesChange = e => {
    //e.target.rows = 1;
    const currentRows = ~~(e.target.scrollHeight / 24);
    this.setState({ des: e.target.value, des_rows: currentRows });
  };
  render() {
    return (
      <div className="group-info">
        <div className="group-info-title">
          <h2>Group Information</h2>
          {this.props.isAdmin ? (
            this.state.edit ? (
              <div className="buttons">
                <button className="but save" onClick={this._save}>
                  Save
                </button>
                <button className="but" onClick={this._cancel}>
                  Cancel
                </button>
              </div>
            ) : (
              <button
                className="but edit"
                onClick={() => {
                  this.setState({ edit: true });
                }}
              >
                Edit
              </button>
            )
          ) : (
            <div />
          )}
        </div>
        <div className="group-info-item">
          <h3>Rules</h3>
          {this.state.edit ? (
            <textarea
              value={this.state.rules}
              onChange={this._handleRulesChange}
              rows={this.state.rule_rows}
              placeholder="Enter your group's rules here..."
            />
          ) : (
            <p>{this.props.rules}</p>
          )}
        </div>
        <div className="group-info-item">
          <h3>Description</h3>
          {this.state.edit ? (
            <textarea
              value={this.state.des}
              onChange={this._handleDesChange}
              rows={this.state.des_rows}
              placeholder="Enter your group's description here..."
            />
          ) : (
            <p>{this.props.des}</p>
          )}
        </div>
      </div>
    );
  }
}

export default withGlobalContext(GroupInfo);
