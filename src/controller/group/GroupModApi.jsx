import makeRequest from "../general/api";
import role from "../general/config";
import {
  url_wrapper,
  addToGroup,
  removeUserFromGroup,
  changeRoleInGroup
} from "./GroupApi";

export default class GroupModApi {
  static getModerator = (group_id, access_token) => {
    /**
     * group_id
     * role: 0 - normal, 1 - mod, 2 - owner
     */
    const filter = JSON.stringify({
      include: ["client"],
      where: {
        role: 1,
        group_id: group_id
      }
    });
    return makeRequest(
      url_wrapper("/GroupMembers?filter=" + filter, access_token),
      {
        method: "GET"
      }
    );
  };

  static addModerator = (data, access_token) => {
    /**
     * user_id
     * group_id
     */
    return addToGroup(
      {
        role: role.moderator,
        user_id: data.user_id,
        group_id: data.group_id
      },
      access_token
    );
  };

  static deleteModerator = (data, access_token) => {
    /**
     * user_id
     * group_id
     */
    return removeUserFromGroup(data, access_token);
  };

  static setModeratorToMember = (data, access_token) => {
    /**
     * user_id
     * group_id
     */
    return changeRoleInGroup(
      {
        role: role.normal,
        user_id: data.user_id,
        group_id: data.group_id
      },
      access_token
    );
  };

  static setModeratorToOwner = (data, access_token) => {
    /**
     * user_id
     * group_id
     */
    return changeRoleInGroup(
      {
        role: role.owner,
        user_id: data.user_id,
        group_id: data.group_id
      },
      access_token
    );
  };
}
