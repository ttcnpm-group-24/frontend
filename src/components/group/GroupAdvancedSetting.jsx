import React from "react";
import "../../css/group/setting-group.css";

class GroupAdvancedSetting extends React.Component {
  render() {
    return (
      <div className="group-info">
        <div className="group-info-title">
          <h2>Advanced Settings</h2>
        </div>
        {this.props.items.map((item, index) => {
          return item.normal ? (
            <div key={index} className="group-info-item-settings">
              <button onClick={item.function} className="but normal">
                {item.name}
              </button>
              <p>{item.des}</p>
            </div>
          ) : (
            <div key={index} className="group-info-item-settings">
              <button onClick={item.function} className="but red">
                {item.name}
              </button>
              <p>{item.des}</p>
            </div>
          );
        })}
      </div>
    );
  }
}

export default GroupAdvancedSetting;
