import React from "react";
import GroupScreenWrapper from "../../components/group/GroupScreenWrapper";
import { withGlobalContext } from "../../components/contexts/GlobalContext";
import PostScreen from "../post/PostScreen";

class GroupHomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id
    };
  }
  renderPosts = () => {
    // Get all the posts of the group and render them
    return <PostScreen />;
  };
  componentDidMount() {
    // Query to get group info, then save to state
    this.props.global.updateCurrentGroup(this.state.id);
  };
  render() { 
    return (
      <GroupScreenWrapper
        id={this.state.id}
        isAdmin={this.props.global.isAdmin}
        name={this.props.global.current_group.name}
        ava={this.props.global.current_group.avatar}
        des={this.props.global.current_group.description}
        content={this.renderPosts()}
      />
    );
  }
}

export default withGlobalContext(GroupHomeScreen);
