/**
 * Props:
 * 1. id
 * 2. isAdmin
 * 3. name
 * 4. ava
 * 5. des
 * 6. content
 */
import React from 'react';
import ScreenWrapper from '../general/ScreenWrapper';
import GroupMemberApi from '../../controller/group/GroupMemberApi';
import {withGlobalContext} from '../../components/contexts/GlobalContext';
import {confirmAlert} from 'react-confirm-alert';
import Navigator from '../../components/general/Navigator';

class GroupScreenWrapper extends React.Component {
  joinGroup = () => {
    confirmAlert({
      title: 'Confirm to join',
      message: 'Are you sure to join this groups?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {
            GroupMemberApi.addMember(
              {
                user_id: this.props.global.user.id,
                group_id: this.props.id,
              },
              this.props.global.access_token,
            ).then(res => {
              if (res.error) {
                alert('Something went wrong');
                return;
              } else {
                Navigator.navigate('/my-groups');
              }
            });
          },
        },
        {
          label: 'No',
        },
      ],
    });
  };
  getButtons = () => {
    var buttons = [
      {
        name: 'Home',
        link: '/groups/home/' + this.props.id,
      },
    ];
    if (this.props.global.isMember) {
      buttons = buttons.concat([
        {
          name: 'Member',
          link: '/groups/members/' + this.props.id,
        },
        {
          name: 'Settings',
          link: '/groups/settings/' + this.props.id,
        },
      ]);
      if (this.props.isAdmin) {
        buttons.push({
          name: 'Moderators',
          link: '/groups/moderators/' + this.props.id,
        });
      }
    } else {
      buttons.push({
        name: 'Join this group',
        function: () => {
          this.joinGroup();
        },
      });
    }
    return buttons;
  };
  render() {
    return (
      <ScreenWrapper
        sidebar_name={this.props.name}
        sidebar_ava={this.props.ava}
        sidebar_des={this.props.des}
        sidebar_buttons={this.getButtons()}
        content={this.props.content}
      />
    );
  }
}

export default withGlobalContext(GroupScreenWrapper);
