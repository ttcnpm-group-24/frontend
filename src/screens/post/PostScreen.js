import React from "react";
import { withGlobalContext } from "../../components/contexts/GlobalContext";
import Post from "../../components/post/Post";
import "../../css/post/list-post.css";
import PostApi from "../../controller/post/PostApi";
import { withRouter } from "react-router-dom";
import logo from "../../logo.svg";

class PostScreen extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      des: "",
      listpost: [

      ]
    }
  }
  getListPost = () => {
    PostApi.getListPost(
      this.props.global.access_token
    )
      .then(data => {
          data.forEach(element => {
            element.link = "/post/" + element.id;
            element.voteCount = 0;
          });
          if(window.location.pathname === '/'){
            var listp = []
            for(let p of data){
              if(p.group_id === null){
                listp.push(p);
              }
            }
            this.setState({listpost : listp})
          }
          else{
            var listp = []
            for(let p of data){
              if(p.group_id === this.props.global.current_group.id){
                listp.push(p);
              }
            }
            this.setState({listpost : listp})
          }
          
      })
      .catch(e => {
        console.error(e);
      });
  };
  renderContent = (item, index) => {
    return (
      <Post 
        key={index}
        item={item}
        menu={[
          {
            name: "Delete",
            function: () => {
              if(item.user_id === this.props.global.user.id){
                PostApi.deletePost(item.id, this.props.global.access_token);
                alert("Delete success");
              }
              else alert("you can't delete")
              this.props.history.push("/");
            }
            
          },
          // {
          //   name: "Edit",
          //   function: () => {
          //     this.props.history.push("/");
          //   }
          // }
        ]}
        up = 
          {() =>{
            if(item.voteCount < 1){
              item.upvote = item.upvote + 1;
              item.voteCount = item.voteCount + 1;
              PostApi.vote(item, this.props.global.access_token)
              .then(item => {this.setState({item})})
              //.then(this.setState({upvote: false, downvote: true}))
            }
        }}
        down = 
        {() =>{
          if (item.voteCount > -1){
            item.upvote = item.upvote - 1;
            item.voteCount = item.voteCount - 1;
            PostApi.vote(item, this.props.global.access_token)
            .then(item => {this.setState({item})})
            //.then(this.setState({downvote: false, upvote: true}))
          }
        }}
        des = {item.content.substr(0,55) + "..."}
        // name = {user_name}
        getName = {() => {
          var user_name = PostApi.getUserName(this.props.item.user_id,this.props.global.access_token);
          // PostApi.getUserName(this.props.item.user_id,this.props.global.access_token)
          // .then(data => {
          //   user_name = data.name;
          // })
          alert(user_name)
          return user_name;
        }}
        time = {item.time.substr(0,10)}
        
      />
    )
  }
  componentWillMount = () => {
    this.getListPost();
  };
  // componentDidMount = () => {
  //   this.getListPost();
  // }
  render() {
    return (
      
      <div className="list-post">
        <div className="post">
          {this.state.listpost.map((item, index) =>
            // item.des = item.content.substr(0,20),
            
            this.renderContent(item, index)
            
          )}
        </div>
      </div>
       
    );
  }
}

export default withGlobalContext(withRouter(PostScreen));
