import React from 'react';
import Sidebar from './Sidebar';
import '../../css/general/screen.css';
import Loading from '../../components/general/Loading';
import {withGlobalContext} from '../../components/contexts/GlobalContext';

class ScreenWrapper extends React.Component {
  render() {
    return this.props.global.loading ? (
      <div className="screen">
        <Loading />
      </div>
    ) : (
      <div className="screen">
        <div className="ad" />
        <Sidebar
          name={this.props.sidebar_name}
          avatar={this.props.sidebar_ava}
          description={this.props.sidebar_des}
          buttons={this.props.sidebar_buttons}
        />
        <div className="content">{this.props.content}</div>
        <div className="ad" />
      </div>
    );
  }
}

export default withGlobalContext(ScreenWrapper);
