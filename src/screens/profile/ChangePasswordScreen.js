import React from 'react';
import {withGlobalContext} from '../../components/contexts/GlobalContext';
import {withRouter} from 'react-router-dom';
import ScreenWrapper from '../../components/general/ScreenWrapper';
import '../../css/profile/profile.css';
import {confirmAlert} from 'react-confirm-alert';
import ProfileApi from '../../controller/profile/ProfileApi';
import Loading from '../../components/general/Loading';

class ChangePasswordScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      password: '',
      old_password: '',
      retype_password: '',
      disabled: true,
      loading: false,
    };
  }
  clear = () => {
    this.setState({
      password: '',
      old_password: '',
      retype_password: '',
      disabled: true,
      loading: false,
    });
  };
  cancel = () => {
    this.clear();
    this.props.history.push('/');
  };
  submit = () => {
    if (this.state.password !== this.state.retype_password) {
      alert('New password does not match');
      return;
    } else {
      confirmAlert({
        title: 'Confirm to change',
        message: 'Are you sure to change your password?',
        buttons: [
          {
            label: 'Yes',
            onClick: () => {
              this.setState({loading: true});
              ProfileApi.changePassword(
                {
                  oldPassword: this.state.old_password,
                  newPassword: this.state.password,
                },
                this.props.global.access_token,
              )
                .then(res => {
                  if (res.error) {
                    alert('Error changing password');
                    console.error(res.error);
                  } else {
                    this.clear();
                    alert('Change password successfully');
                  }
                })
                .catch(e => {
                  console.error(e);
                })
                .then(() => {
                  this.setState({loading: false});
                });
            },
          },
          {
            label: 'No',
          },
        ],
      });
    }
  };
  renderContent = () => {
    return (
      <div className="edit-profile">
        <h2 className="page-title" style={{border: 'none'}}>
          PASSWORD CHANGING
        </h2>
        <h4 className="page-title">Old Password</h4>
        <div className="input">
          <input
            type="password"
            name="old_password"
            placeholder="Enter your current password here"
            value={this.state.old_password}
            onChange={e => {
              this.setState({old_password: e.target.value});
            }}
          />
        </div>
        <h4 className="page-title">New password</h4>
        <div className="input">
          <input
            type="password"
            name="password"
            placeholder="Enter your new password here"
            value={this.state.password}
            onChange={e => {
              this.setState({password: e.target.value});
            }}
          />
        </div>
        <h4 className="page-title">Retype new password</h4>
        <div className="input">
          <input
            type="password"
            name="password"
            placeholder="Retype your new password here"
            value={this.state.retype_password}
            onChange={e => {
              this.setState({retype_password: e.target.value}, () => {
                if (
                  this.state.retype_password === this.state.password &&
                  this.state.retype_password !== ''
                ) {
                  this.setState({disabled: false});
                } else {
                  this.setState({disabled: true});
                }
              });
            }}
          />
        </div>
        <div className="buttons">
          <button className="cancel" onClick={this.cancel}>
            Cancel
          </button>
          <button
            className="submit"
            onClick={this.submit}
            disabled={this.state.disabled}>
            Submit
          </button>
        </div>
      </div>
    );
  };
  render() {
    return (
      <ScreenWrapper
        sidebar_name={this.props.global.user.name}
        sidebar_ava={this.props.global.user.avatar}
        sidebar_des={this.props.global.user.description}
        sidebar_buttons={this.props.global.getButtons()}
        content={this.state.loading ? <Loading /> : this.renderContent()}
      />
    );
  }
}

export default withGlobalContext(withRouter(ChangePasswordScreen));
