import React from 'react';
import {withGlobalContext} from '../../components/contexts/GlobalContext';
import ScreenWrapper from '../../components/general/ScreenWrapper';
import '../../css/group/my-group.css';
import GroupListItem from '../../components/group/GroupListItem';
import GroupApi from '../../controller/group/GroupApi';
import logo from '../../logo.svg';
import {server_url} from '../../controller/general/config';
import Loading from '../../components/general/Loading';

class MyGroupScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
    };
  }
  getMyGroups = () => {
    // Query to get groups
    // {
    //   name: "This is the first group",
    //   ava: this.props.global.user.ava,
    //   des: "This is the first group description",
    //   link: "/groups/home/1"
    // }
    return GroupApi.getMyGroup(
      this.props.global.user.id,
      this.props.global.access_token,
    )
      .then(data => {
        if (data.error) {
          alert('Please login');
          return;
        } else {
          data.forEach(element => {
            var newEle = element;
            GroupApi.getAvatar(element.id, this.props.global.access_token)
              .then(ava => {
                if (ava.url) {
                  newEle.avatar = server_url + ava.url;
                }
              })
              .then(() => {
                newEle.link = '/groups/home/' + element.id;
              })
              .then(() => {
                var newGroup = this.state.data;
                newGroup.push(element);
                this.setState({data});
              });
          });
        }       
      })
      .catch(e => {
        console.error(e);
      });
  };
  componentWillMount = () => {
    this.getMyGroups().then(() => {
      this.setState({loading: false});
    });
  };
  renderContent = (item, index) => {
    return (
      <GroupListItem
        key={index}
        item={item}
        menu={[
          {
            name: 'Group Profile',
            function: () => {
              this.props.history.push(item.link);
            },
          },
        ]}
      />
    );
  };
  render() {
    return (
      <ScreenWrapper
        sidebar_name={this.props.global.user.name}
        sidebar_ava={this.props.global.user.avatar}
        sidebar_des={this.props.global.user.description}
        sidebar_buttons={this.props.global.getButtons()}
        content={
          this.state.loading ? (
            <Loading />
          ) : (
            <div className="my-group-wrapper">
              <div className="my-group">
                {this.state.data.map((item, index) =>
                  this.renderContent(item, index),
                )}
              </div>
            </div>
          )
        }
      />
    );
  }
}

export default withGlobalContext(MyGroupScreen);
