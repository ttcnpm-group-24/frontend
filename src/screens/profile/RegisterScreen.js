import React from "react";
import { withGlobalContext } from "../../components/contexts/GlobalContext";
import { withRouter } from "react-router-dom";
import ScreenWrapper from "../../components/general/ScreenWrapper";
import "../../css/profile/register.css";

class RegisterScreen extends React.Component {
      constructor(props) {
        super(props);
        this.state = {
          name: ""
        };
      }
      cancel = () => {
        this.props.history.push("/");
      };
      getInput = name => {
        if (name === "username") {
          return this.state.username;
        }
      };
      renderContent = () => {
        return (
          <div className="register">
            <div className="dialog">
              <div className="info">
                <div className="content">
                  <h1>Register</h1>
                </div>
                <div className="content">
                  <div className="title">
                    <h4>Username</h4>
                  </div>
                  <input
                    type="text"
                    name="username"
                    placeholder="Enter your username"
                  />
                </div>
                <div className="content">
                  <div className="title">
                    <h4>Password</h4>
                  </div>
                  <input
                    type="password"
                    name="password"
                    placeholder="Enter your password"
                  />
                </div>
                <div className="content">
                  <div className="title">
                    <h4>Full Name</h4>
                  </div>
                  <input
                    type="text"
                    name="fullname"
                    placeholder="Enter your full name"
                  />
                </div>
                <div className="content">
                  <div className="title">
                    <h4> Email</h4>
                  </div>
                  <input
                    type="text"
                    name="email"
                    placeholder="Enter your email"
                  />
                </div>
                <div className="content" id="buttons">
                  <button className="cancel" onClick={this.cancel}>
                    Cancel
                  </button>
                  <button className="submit">
                  Register</button>
                </div>
              </div>
            </div>
          </div>
        );
      };
      render() {
        return (
          <ScreenWrapper
            sidebar_name={this.props.global.user.name}
            sidebar_ava={this.props.global.user.ava}
            sidebar_des={this.props.global.user.des}
            sidebar_buttons={this.props.global.getButtons()}
            content={this.renderContent()}
          />
        );
      }
    }

export default withGlobalContext(withRouter(RegisterScreen));
