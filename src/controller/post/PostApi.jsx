import makeRequest from "../general/api";



export const url_wrapper = (url, access_token) => {
  if (access_token) {
    if (url.includes("?")) {
      url = url + "&access_token=" + access_token;
    } else {
      url = url + "?access_token=" + access_token;
    }
  }
  return url;
};
class PostApi{
    static createPost = (data, access_token) => {
        /**
         * id
         * title
         * description
         * content
         */
        
        return makeRequest(url_wrapper("/Posts", access_token), {
          method: "POST",
          body: JSON.stringify(data)
        })
        .then(response => {
          console.log(response);
        })
      };
      static getListPost = (access_token) => {
        return makeRequest(
          url_wrapper("/Posts", access_token),
          {
            method: "GET"
          }
        );
      };
      static getPost = (id, access_token) => {
        return makeRequest(
          url_wrapper("/Posts/" + id, access_token),
          {
            method: "GET"
          }
        );
      };
      static deletePost = (id, access_token) => {
        return makeRequest(url_wrapper("/Posts/" + id, access_token), {
          method: "DELETE"
        });
      };
      static vote = (data, access_token) => {
        return makeRequest(url_wrapper("/Posts/" + data.id, access_token), {
          method: "PUT",
          body: JSON.stringify(data)
        });
      };
      static getUserName = (id, access_token) => {
        return makeRequest(url_wrapper("/Clients/" + id, access_token), {
          method: "GET"
        });
      };
}


export default PostApi;