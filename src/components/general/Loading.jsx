import React from 'react';
import '../../css/general/loading.css';
import CircularProgress from '@material-ui/core/CircularProgress';

class Loading extends React.Component {
  render() {
    return (
      <div className="loading">
        <CircularProgress />
      </div>
    );
  }
}

export default Loading;
